package br.com.facec;

import java.util.Collections;
import java.util.List;

public class Impressora {

    public static void imprimir(Imprimivel imprimivel) {
        imprimir(Collections.singletonList(imprimivel));
    }

    public static void imprimir(List<Imprimivel> lista) {
        for (Imprimivel imprimivel : lista) {
            imprimivel.imprimir();
        }
    }


}
