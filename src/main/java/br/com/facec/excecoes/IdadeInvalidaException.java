package br.com.facec.excecoes;

public class IdadeInvalidaException extends RuntimeException {
    public IdadeInvalidaException(String message) {
        super(message);
    }
}
