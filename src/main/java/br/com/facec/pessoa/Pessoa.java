package br.com.facec.pessoa;

import br.com.facec.excecoes.IdadeInvalidaException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Pessoa {
    private final String nome;
    private final List<Contato> contatos = new ArrayList<>();
    private final List<Endereco> enderecos = new ArrayList<>();

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public abstract String imprimirDadosEspecificos();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("nome: ").append(nome).append(" \n");

        sb.append("Telefones: ").append(" \n");
        for (Telefone telefone : getTelefones()) {
            sb.append(telefone.numero)
                    .append(" - ")
                    .append(telefone.tipo)
                    .append(" \n");
        }

        sb.append(" \n");
        sb.append("Emails: ");
        for (Email email : getEmail()) {
            sb.append(email.email)
                    .append(" \n");
        }

        sb.append("Endereços: ").append(" \n");
        for (Endereco endereco : this.enderecos) {
            sb.append(endereco.logradouro)
                    .append(" - ")
                    .append(endereco.tipo)
                    .append(" - ")
                    .append(endereco.cidade.nome)
                    .append(" - ")
                    .append(endereco.cidade.estado)
                    .append(" - ")
                    .append(endereco.cidade.getPais())
                    .append(" \n");
        }

        sb.append(imprimirDadosEspecificos());

        return sb.toString();
    }

    public void adicionaEmail(Email email) {
        if (email != null) {
            email.validar();
            this.contatos.add(email);
        }
    }

    public List<Telefone> getTelefones() {
        return contatos.stream()
                .filter(c -> c instanceof Telefone)
                .map(contato -> (Telefone)contato)
                .collect(Collectors.toList());
    }

    public List<Email> getEmail() {
        List<Email> emails = new ArrayList<>();
        for (Contato contato : contatos) {
            if (contato instanceof Email) {
                emails.add((Email) contato);
            }
        }

        return emails;
    }

    public void adicionaTelefone(Telefone telefone) {
        contatos.add(telefone);
    }

    public void adicionaEndereco(Endereco endereco) {
        enderecos.add(endereco);
    }
}
