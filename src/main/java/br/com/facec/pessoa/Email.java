package br.com.facec.pessoa;

import br.com.facec.excecoes.EmailInvalidoExpcetion;

public class Email implements Contato {

    public String email;

    public void validar() {
        if(email != null) {
            if (email.isEmpty() || !email.contains("@")) {
                throw new EmailInvalidoExpcetion("email invalido");
            }
        }
    }

}
