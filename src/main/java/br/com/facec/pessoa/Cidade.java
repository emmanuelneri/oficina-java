package br.com.facec.pessoa;

public class Cidade {

    public String nome;
    public Estado estado;

    public Pais getPais() {
        if(estado == null) {
            return null;
        }

        return this.estado.pais;
    }
}
