package br.com.facec.pessoa;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum TipoCliente {
    FISICA{
        @Override
        public BigDecimal calcularMargemDeVenda(BigDecimal valor) {
            return calcular(valor, 0.15);
        }
    },
    JURIDICA{
        @Override
        public BigDecimal calcularMargemDeVenda(BigDecimal valor) {
            return calcular(valor, 0.10);
        }
    };

    public abstract BigDecimal calcularMargemDeVenda(BigDecimal valor);

    private static BigDecimal calcular(BigDecimal valor, double margem) {
        return valor.multiply(BigDecimal.valueOf(margem));
    }

}
