package br.com.facec.pessoa;

import br.com.facec.Imprimivel;

public class Fornecedor extends Pessoa implements Imprimivel {

    private final Cnpj cnpj;

    public Fornecedor(String nome, String cnpj) {
        super(nome);
        this.cnpj = new Cnpj(cnpj);
    }

    @Override
    public String imprimirDadosEspecificos() {
        StringBuilder sb = new StringBuilder();
        sb.append("CNPJ: ").append(cnpj).append("\n");
        return sb.toString();
    }

    @Override
    public void imprimir() {
        System.out.println(this);
    }
}
