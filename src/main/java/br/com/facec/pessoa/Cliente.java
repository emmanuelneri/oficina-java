package br.com.facec.pessoa;

import br.com.facec.excecoes.IdadeInvalidaException;

import java.util.Random;

public class Cliente extends Pessoa {

    private Documento documento;
    private String rg;
    private Integer idade;
    private final int numeroCliente;
    private TipoCliente tipo;

    private Cliente(String nome) {
        super(nome);
        this.numeroCliente = new Random().nextInt();
    }
    public static Cliente criaClienteTipoFisico(String nome, String cpf) {
        Cliente cliente = new Cliente(nome);
        cliente.documento = new Cpf(cpf);
        cliente.tipo = TipoCliente.FISICA;
        return cliente;
    }

    public static Cliente criaClienteTipoJuridico(String nome, String cnpj) {
        Cliente cliente = new Cliente(nome);
        cliente.documento = new Cnpj(cnpj);
        cliente.tipo = TipoCliente.JURIDICA;
        return cliente;
    }

    @Override
    public String imprimirDadosEspecificos() {
        StringBuilder sb = new StringBuilder();
        sb.append("Documento: ").append(documento).append("\n");
        sb.append("numeroCliente: ").append(numeroCliente).append("\n");
        sb.append("rg: ").append(rg).append(" \n");
        sb.append("idade: ").append(idade).append(" \n");
        return sb.toString();
    }

    public void setIdade(Integer idade) {
        if (idade != null && idade <= 0) {
            throw new IdadeInvalidaException("Idade inválida");
        }

        this.idade = idade;
    }


    public TipoCliente getTipo() {
        return tipo;
    }

    public int getNumeroCliente() {
        return numeroCliente;
    }
}
