package br.com.facec.pessoa;

public enum Tipo {
    COMERCIAL,
    RESIDENCIAL,
    PARTICULAR,
    OUTROS;
}
