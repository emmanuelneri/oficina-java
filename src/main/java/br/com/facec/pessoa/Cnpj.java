package br.com.facec.pessoa;

public final class Cnpj extends Documento {

    public Cnpj(String valor) {
        super(valor);
    }

    @Override
    public void validar() {
        if (getValor() == null || getValor().isEmpty()) {
            throw new RuntimeException("o CNPJ deve ser preenchido");
        }

        if (getValor().length() != 14) {
            throw new RuntimeException("CNPJ invalido");
        }
    }
}
