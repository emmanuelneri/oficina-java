package br.com.facec.pessoa;

public final class Cpf extends Documento{

    public Cpf(String valor) {
        super(valor);
    }

    @Override
    public void validar() {
        if (getValor() == null || getValor().isEmpty()) {
            throw new RuntimeException("o CPF deve ser preenchido");
        }

        if (getValor().length() != 12) {
            throw new RuntimeException("CPF invalido");
        }
    }
}
