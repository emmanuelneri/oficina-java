package br.com.facec.pessoa;

public abstract class Documento {

    private final String valor;

    public Documento(String valor) {
        this.valor = valor;
        validar();
    }

    public abstract void validar();

    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return this.valor;
    }

}
