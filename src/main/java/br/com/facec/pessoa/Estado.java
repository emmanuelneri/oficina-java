package br.com.facec.pessoa;

public enum Estado {
    PR(Pais.BR),
    SP(Pais.BR),
    SC(Pais.BR);

    public Pais pais;

    Estado(Pais pais) {
        this.pais = pais;
    }
}
