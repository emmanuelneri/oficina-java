package br.com.facec.pedido;

import java.math.BigDecimal;

public class ItemPedido {

    public Produto produto;
    public int quantidade;

    public ItemPedido(Produto produto,
                      int quantidade) {
        this.produto = produto;
        this.quantidade = quantidade;
    }

    public BigDecimal getValorTotal() {
        return produto.getPreco().multiply(
                BigDecimal.valueOf(quantidade));
    }
}
