package br.com.facec.pedido;

public class FechamentoPedido {

    public static void fecharPedido(Pedido pedido) {
        pedido.concluirPedido();
    }

}
