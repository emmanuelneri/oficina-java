package br.com.facec.pedido;

import br.com.facec.pessoa.Cliente;
import br.com.facec.pessoa.Pessoa;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PedidoVenda extends Pedido {

    private Cliente cliente;

    public PedidoVenda(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void concluirPedido() {
        super.concluirPedido();

        BigDecimal valorMargem = cliente.getTipo()
                .calcularMargemDeVenda(valor);

        this.valor = valor.add(valorMargem);
    }

    @Override
    protected Pessoa getPessoa() {
        return cliente;
    }


    @Override
    public void imprimir() {
        System.out.println(this);
    }
}
