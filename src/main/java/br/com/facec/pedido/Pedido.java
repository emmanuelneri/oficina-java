package br.com.facec.pedido;

import br.com.facec.Imprimivel;
import br.com.facec.pessoa.Pessoa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class Pedido implements Imprimivel {

    protected BigDecimal valor = BigDecimal.ZERO;
    private final List<ItemPedido> items = new ArrayList<ItemPedido>();
    private StatusPedido status = StatusPedido.ABERTO;

    protected abstract Pessoa getPessoa();

    public void adicionarProduto(Produto produto, int quantidade) {
        if (quantidade <= 0) {
            throw new RuntimeException("a quantidade de produto tem que ser maior que zero");
        }

        if (produto.getPreco().compareTo(BigDecimal.ZERO) < 0) {
            throw new RuntimeException("não pode ser adicionado produtos com valor menor que zero");
        }

        ItemPedido item = new ItemPedido(produto, quantidade);
        this.items.add(item);
        this.valor = this.valor.add(item.getValorTotal());
    }

    public void removerProduto(Produto produto) {
        ItemPedido itemARemover = null;
        for (ItemPedido item : this.items) {
            if (item.produto.getCodigo().equals(produto.getCodigo())) {
                itemARemover = item;
                break;
            }
        }

        if (itemARemover != null) {
            this.items.remove(itemARemover);
            this.valor = this.valor
                    .subtract(itemARemover.getValorTotal());
        }
    }

    public void concluirPedido() {
        if (getPessoa() == null) {
            throw new RuntimeException("Pedido não pode ser concluído sem uma pessoa");
        }

        if (valor.compareTo(BigDecimal.ZERO) < 0) {
            throw new RuntimeException("Pedido não pode ser concluído com valor total que zero");
        }

        if (items.size() == 0) {
            throw new RuntimeException("Pedido não pode ser concluído sem items");
        }

        this.status = StatusPedido.CONCLUIDO;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Pessoa: ").append(getPessoa().getNome()).append("\n");
        sb.append("Status: ").append(status).append("\n");
        sb.append("Valor: ").append(valor).append("\n");

        for (ItemPedido item : items) {
            sb.append("Item: ")
                    .append(item.produto.getDescricao())
                    .append(" - ")
                    .append(item.quantidade)
                    .append("\n");

        }

        return sb.toString();
    }
}
