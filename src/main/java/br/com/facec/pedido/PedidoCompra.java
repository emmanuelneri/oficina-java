package br.com.facec.pedido;

import br.com.facec.pessoa.Fornecedor;
import br.com.facec.pessoa.Pessoa;

public class PedidoCompra extends Pedido {

   private Fornecedor fornecedor;

    public PedidoCompra(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    @Override
    protected Pessoa getPessoa() {
        return fornecedor;
    }

    public void imprimir() {
        System.out.println(this);
    }
}
