package br.com.facec.pedido;

public enum StatusPedido {
    ABERTO,
    CONCLUIDO;
}
