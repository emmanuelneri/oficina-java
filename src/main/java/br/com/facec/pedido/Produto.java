package br.com.facec.pedido;

import java.math.BigDecimal;

public class Produto {

    private String codigo;
    private String descricao;
    private BigDecimal preco;

    public Produto(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        if(preco == null || BigDecimal.ZERO.equals(preco)) {
            throw new RuntimeException("o preço do produto não pose ser nulo ou zero");
        }

        if (preco.compareTo(BigDecimal.ZERO) < 0) {
            throw new RuntimeException("o produto não pode ter preço menor que zero");
        }

        this.preco = preco;
    }
}
