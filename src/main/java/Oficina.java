import br.com.facec.Impressora;
import br.com.facec.Imprimivel;
import br.com.facec.excecoes.EmailInvalidoExpcetion;
import br.com.facec.excecoes.IdadeInvalidaException;
import br.com.facec.pedido.FechamentoPedido;
import br.com.facec.pedido.PedidoCompra;
import br.com.facec.pedido.PedidoVenda;
import br.com.facec.pedido.Produto;
import br.com.facec.pessoa.Cidade;
import br.com.facec.pessoa.Cliente;
import br.com.facec.pessoa.Email;
import br.com.facec.pessoa.Endereco;
import br.com.facec.pessoa.Estado;
import br.com.facec.pessoa.Fornecedor;
import br.com.facec.pessoa.Telefone;
import br.com.facec.pessoa.Tipo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Oficina {

    public static void main(String[] args) {
        System.out.println("Iniciando programa");
        executaSistemaDePedidos();
        System.out.println("Finalizado programa");
//        imprimir();
    }

    private static void imprimir() {
        Fornecedor fornecedor = new Fornecedor("Forncedor", "00000000000000");
        PedidoCompra pedidoCompra = new PedidoCompra(fornecedor);

        List<Imprimivel> lista = new ArrayList<>();
        lista.add(pedidoCompra);
        lista.add(fornecedor);

        Impressora.imprimir(lista);
    }

    private static void executaSistemaDePedidos() {
        System.out.println("Iniciando sistema de pedidos");
        try {
            Cidade cidade = new Cidade();
            cidade.nome = "Cianorte";
            cidade.estado = Estado.PR;

            Produto celular = new Produto("001", "Celular Samsung");
            celular.setPreco(BigDecimal.valueOf(2000));

            Produto acessorio = new Produto("1010", "Capa de celular");
            acessorio.setPreco(BigDecimal.valueOf(10));

            Fornecedor fornecedor = criarFornecedor(cidade);
            Map<Produto, Integer> produtosPorQuantidadeCompra = new HashMap<Produto, Integer>();
            produtosPorQuantidadeCompra.put(celular, 10);
            produtosPorQuantidadeCompra.put(acessorio, 50);
            criaPedidoCompra(fornecedor, produtosPorQuantidadeCompra);

            Cliente cliente = criaCliente(cidade);
            Map<Produto, Integer> produtosPorQuantidade = new HashMap<Produto, Integer>();
            produtosPorQuantidade.put(celular, 1);
            produtosPorQuantidade.put(acessorio, 2);

            criaPedidoVenda(cliente, produtosPorQuantidade);

        } catch (Exception ex) {
            System.out.println("ERRO - nosso sistema falhou " + ex.getMessage());
            ex.printStackTrace();
        }

        System.out.println("Finalizado sistema de pedidos");
    }

    private static Cliente criaCliente(Cidade cidade) {
        System.out.println("-------PESSOA--------");

        Endereco endereco = new Endereco();
        endereco.logradouro = "avenida goais";
        endereco.tipo = Tipo.RESIDENCIAL;
        endereco.cidade = cidade;

        Telefone telefoneComercial = new Telefone();
        telefoneComercial.ddd = "44";
        telefoneComercial.numero = "99999292";
        telefoneComercial.tipo = Tipo.COMERCIAL;

        Telefone telefoneResidencial = new Telefone();
        telefoneResidencial.ddd = "44";
        telefoneResidencial.numero = "32433553";
        telefoneResidencial.tipo = Tipo.RESIDENCIAL;

        Cliente cliente = Cliente.criaClienteTipoFisico("Nome da Pessoa", "111111111111");

        try {
            cliente.setIdade(20);
        } catch (IdadeInvalidaException ex) {
            System.out.println("idade inválida");
        }

        Email email = new Email();
        email.email = "teste@facec.com.br";
        try {
            cliente.adicionaEmail(email);
        } catch (EmailInvalidoExpcetion ex) {
            System.out.println("email inválido");
        }

        cliente.adicionaTelefone(telefoneComercial);
        cliente.adicionaTelefone(telefoneResidencial);
        cliente.adicionaEndereco(endereco);

        System.out.println();
        return cliente;
    }

    private static void criaPedidoVenda(Cliente cliente, Map<Produto, Integer> produtosPorQuantidade) {
        System.out.println("-------PEDIDO VENDA--------");
        Produto celular = new Produto("001", "Celular Samsung");
        celular.setPreco(BigDecimal.valueOf(2000));

        Produto acessorio = new Produto("1010", "Capa de celular");
        acessorio.setPreco(BigDecimal.valueOf(10));

        PedidoVenda pedido = new PedidoVenda(cliente);

        for(Map.Entry<Produto, Integer> produtoPorQuantidade : produtosPorQuantidade.entrySet()) {
            pedido.adicionarProduto(produtoPorQuantidade.getKey(), produtoPorQuantidade.getValue());
        }

        FechamentoPedido.fecharPedido(pedido);
        pedido.imprimir();
    }

    public static Fornecedor criarFornecedor(Cidade cidade) {
        Fornecedor fornecedor = new Fornecedor("Fornecedor", "00000000000000");

        Endereco endereco = new Endereco();
        endereco.logradouro = "avenida goais";
        endereco.tipo = Tipo.COMERCIAL;
        endereco.cidade = cidade;

        Telefone telefoneComercial = new Telefone();
        telefoneComercial.ddd = "44";
        telefoneComercial.numero = "943034034";
        telefoneComercial.tipo = Tipo.COMERCIAL;

        Email email = new Email();
        email.email = "teste@facec.com.br";
        try {
            fornecedor.adicionaEmail(email);
        } catch (EmailInvalidoExpcetion ex) {
            System.out.println("email inválido");
        }

        fornecedor.adicionaTelefone(telefoneComercial);
        fornecedor.adicionaEndereco(endereco);

        fornecedor.imprimir();

        return fornecedor;
    }

    private static void criaPedidoCompra(Fornecedor fornecedor, Map<Produto, Integer> produtosPorQuantidade) {
        System.out.println("-------PEDIDO Compra--------");

        PedidoCompra pedido = new PedidoCompra(fornecedor);
        for(Map.Entry<Produto, Integer> produtoPorQuantidade : produtosPorQuantidade.entrySet()) {
            pedido.adicionarProduto(produtoPorQuantidade.getKey(), produtoPorQuantidade.getValue());
        }

        FechamentoPedido.fecharPedido(pedido);

        System.out.println(pedido);
    }
}
